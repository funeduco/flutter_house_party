import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './widgets/bottom_navbar.dart';
import './screens/live_videos_screen.dart';
import './providers/videos.dart';
import './screens/more_music_screen.dart';
import './providers/routing.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Videos(),
        ),
        ChangeNotifierProvider.value(
          value: Routing(),
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(
          primaryColor: Color(0xFFE52020),
        ),
        home: BottomnavBar(),
        routes: {
          LiveVideosScreen.routeName: (ctx) => LiveVideosScreen(),
          MoreMusicScreen.routeName: (ctx) => MoreMusicScreen()
        },
      ),
    );
  }
}
