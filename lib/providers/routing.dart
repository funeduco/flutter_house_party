import 'package:flutter/material.dart';

class Routing with ChangeNotifier{
    int index = 0;

    void pages(int pageIndex) {
      index = pageIndex;
      notifyListeners();
    }

    int get pageIndex{
      return index;
    }
}