import 'package:flutter/foundation.dart';

class Video with ChangeNotifier{
  final String id;
  final String title;
  final String artist;
  final DateTime duration;
  final String imagePath;

  Video({
    @required this.id,
    @required this.title,
    @required this.artist,
    @required this.duration,
    @required this.imagePath
  });
}