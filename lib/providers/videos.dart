import 'package:flutter/material.dart';
import './video.dart';

class Videos with ChangeNotifier {
  List<Video> _items = [
    Video(
      id: DateTime.now().toString(),
      title: 'Home Brewed',
      artist: 'Black coffee',
      duration: DateTime.now().add(Duration(hours: 2)),
      imagePath: 'assets/images/blac.jpg',
    ),
    Video(
      id: DateTime.now().toString(),
      title: 'LockDownHouseParty',
      artist: 'Kabza de Small and Maphorisa',
      duration: DateTime.now().add(Duration(hours: 2)),
      imagePath: 'assets/images/kabza.jpg',
    ),
    Video(
      id: DateTime.now().toString(),
      title: 'Ceega 165 (100% Local)',
      artist: 'Ceega wa meropa',
      duration: DateTime.now().add(Duration(hours: 2)),
      imagePath: 'assets/images/cwm.jpg',
    ),
    Video(
      id: DateTime.now().toString(),
      title: 'Godson - Mr TimeADeep',
      artist: 'Nostalgic mode activated',
      duration: DateTime.now().add(Duration(hours: 2)),
      imagePath: 'assets/images/godfathers1.jpg',
    ),
    Video(
      id: DateTime.now().toString(),
      title: 'Godbabe - Ros33ta',
      artist: 'Nostalgic mode activated',
      duration: DateTime.now().add(Duration(hours: 2)),
      imagePath: 'assets/images/godfathers2.jpg',
    ),
    Video(
      id: DateTime.now().toString(),
      title: 'House Afrika',
      artist: 'Vinny\s House',
      duration: DateTime.now().add(Duration(hours: 2)),
      imagePath: 'assets/images/afrika.png',
    ),
  ];

  List<Video> get items {
    return [..._items];
  }
}
