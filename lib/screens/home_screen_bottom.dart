import 'package:flutter/material.dart';
import 'package:flutter_house_party/providers/video.dart';
import 'package:provider/provider.dart';

import '../providers/videos.dart';
import '../providers/routing.dart';

class HomeScreenBottomPart extends StatelessWidget {
  List<Widget> playlist(List<Video> videos) {
    List<Widget> musicList = new List();

    for (int i = 0; i < videos.length; i++) {
      var musicItem = Padding(
        padding: EdgeInsets.symmetric(vertical: 25.0, horizontal: 12.0),
        child: Container(
          height: 220.0,
          width: 135.0,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black12,
                    blurRadius: 10.0,
                    offset: Offset(0.0, 10.0))
              ]),
          child: Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0)),
                child: Image.asset(
                  videos[i].imagePath,
                  width: double.infinity,
                  height: 130.0,
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0, left: 8.0, right: 8.0),
                child: Text(videos[i].title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 13.0, fontFamily: "SF-Pro-Display-Bold")),
              ),
            ],
          ),
        ),
      );
      musicList.add(musicItem);
    }
    return musicList;
  }

  @override
  Widget build(BuildContext context) {
    final videosData = Provider.of<Videos>(context).items;
    final routing = Provider.of<Routing>(context);
    return new Container(
      height: 360.0,
      margin: EdgeInsets.only(left: 65.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Watch now",
                  style: TextStyle(
                      fontSize: 22.0, fontFamily: "SF-Pro-Display-Bold"),
                ),
                FlatButton(
                  child: Text("View more"),
                  onPressed: () {
                    routing.pages(2);
                  },
                )
              ],
            ),
          ),
          Container(
            height: 250.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: playlist(videosData),
            ),
          )
        ],
      ),
    );
  }
}
