import 'package:flutter/material.dart';
import 'package:flutter_house_party/widgets/video_item.dart';
import 'package:provider/provider.dart';

import '../providers/videos.dart';

class MoreMusicScreen extends StatelessWidget {
  static const routeName = '/more-music';
  @override
  Widget build(BuildContext context) {
    final vieodsData = Provider.of<Videos>(context).items;
    return Scaffold(
      appBar: AppBar(
        title: Text("More music"),
      ),
      body: GridView.builder(
        padding: const EdgeInsets.all(10.0),
        itemCount: vieodsData.length,
        itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
          value: vieodsData[i],
          child: VideoItem(),
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
        ),
      ),
    );
  }
}
