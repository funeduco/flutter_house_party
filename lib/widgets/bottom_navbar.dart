import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/routing.dart';
import '../screens/home_screen.dart';
import '../screens/search_screen.dart';
import '../screens/more_music_screen.dart';
import '../screens/live_videos_screen.dart';

class BottomnavBar extends StatefulWidget {
  @override
  _BottomnavBarState createState() => _BottomnavBarState();
}

class _BottomnavBarState extends State<BottomnavBar> {
    List<Map<String, Object>> _pages = [
      {
        'page': HomeScreen(),
        'title': 'Home',
      },
      {
        'page': SearchScreen(),
        'title': 'Search',
      },
      {
        'page': MoreMusicScreen(),
        'title': 'Music',
      },
      {
        'page': LiveVideosScreen(),
        'title': 'Live',
      },
    ];
  @override
  Widget build(BuildContext context) {
    return Consumer<Routing>(
      builder: (ctx, _myPages, _) => Scaffold(
        body:
            _pages[_myPages.pageIndex]['page'],
        bottomNavigationBar: BottomNavigationBar(
          onTap: (index) {
            setState(() {
              _myPages.pages(index);
            });
          },
          unselectedItemColor: Colors.black54,
          selectedItemColor: Color(0xFFE52020),
          currentIndex: _myPages.pageIndex,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text("Home"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              title: Text("Search"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.library_music),
              title: Text("More music"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.video_library),
              title: Text("Live video\'s"),
            ),
          ],
        ),
      ),
    );
  }
}
