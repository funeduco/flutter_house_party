import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/video.dart';

class VideoItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final videoData = Provider.of<Video>(context);
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          onTap: () {},
          child: Image.asset(
            videoData.imagePath,
            fit: BoxFit.cover,
          ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          leading: IconButton(
            icon: Icon(Icons.favorite_border),
            onPressed: () {},
          ),
          title: Text(
            videoData.title,
            textAlign: TextAlign.center,
          ),
          trailing: IconButton(
            icon: Icon(Icons.share),
            onPressed: () {},
          ),
        ),
      ),
    );
  }
}
